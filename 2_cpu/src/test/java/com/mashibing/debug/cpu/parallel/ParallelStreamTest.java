package com.mashibing.debug.cpu.parallel;

import com.mashibing.debug.cpu.TimeTool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * 自定义
 */
public class ParallelStreamTest {

    public static void main(String[] args) {
        int num = 1000000;
        List<Integer> list = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            list.add(i);
        }

        TimeTool.check("", () -> {
            List<Integer> collect = list.stream()
                    .filter(item -> item > 100)
                    .collect(Collectors.toList());
        });

        ForkJoinPool forkJoinPool = new ForkJoinPool(20);

        TimeTool.check("", () -> {
            try {
                List<Integer> integers = forkJoinPool.submit(() ->
                        list
                                .parallelStream()
                                .filter(item -> item > 100)
                                .collect(Collectors.toList())
                ).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        forkJoinPool.shutdown();
    }


}

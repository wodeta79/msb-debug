package com.mashibing.debug.cpu.parallel;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

public class ForkJoinPoolTest extends RecursiveTask<Long> {

    private static final int THRESHOLD = 500;
    long[] array;
    int start;
    int end;

    ForkJoinPoolTest(long[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }

    public static void main(String[] args) {
        long[] arrays = new long[1000];
        long expecteSum = 0;
        for (int i = 0; i < arrays.length; i++) {
            arrays[i] = i;
            expecteSum = expecteSum + arrays[i];
        }
        System.out.println(expecteSum);

        ForkJoinPoolTest forkJoinPoolTest = new ForkJoinPoolTest(arrays, 0, arrays.length);
        long startTime = System.currentTimeMillis();
        Long result = ForkJoinPool.commonPool().invoke(forkJoinPoolTest);
        long endTime = System.currentTimeMillis();


        System.out.println("fork/join sum:" + result + ",耗时：" + (endTime - startTime));
    }

    @Override
    protected Long compute() {
        //如果任务太小
        if (end - start <= THRESHOLD) {
            System.out.println("start=" + start + ",end=" + end);
            long sum = 0;
            for (int i = start; i < end; i++) {
                sum = sum + this.array[i];
                try {
                    TimeUnit.MILLISECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return sum;
        }
        int middle = (end + start) / 2;
        ForkJoinPoolTest joinPoolTest1 = new ForkJoinPoolTest(this.array, start, middle);
        ForkJoinPoolTest joinPoolTest2 = new ForkJoinPoolTest(this.array, middle, end);
        invokeAll(joinPoolTest1, joinPoolTest2);
        Long result1 = joinPoolTest1.join();
        Long result2 = joinPoolTest2.join();
        long result = result1 + result2;
        System.out.println("result = " + (result1 + result2));
        return result;
    }

}
